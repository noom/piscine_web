#!/usr/bin/php
<?php
function ft_split($tosplit) {
	$tosplit = trim(preg_replace("/[\\x00-\\x20]+/", " ", $tosplit));
	$exploded = explode(" ", $tosplit);
	return $exploded;
}

if ($argc < 2)
	exit;
$splited = ft_split($argv[1]);
if (($len = count($splited)) > 1) {
	$tmp = $splited[0];
	array_shift($splited);
	$splited[$len - 1] = $tmp;
}
foreach ($splited as $key => $val) {
	echo("$val");
	if ($key != $len - 1)
		echo(" ");
}
echo("\n");
?>
