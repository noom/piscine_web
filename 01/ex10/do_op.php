#!/usr/bin/php
<?php
if ($argc < 4) {
	print("Incorrect Parameters\n");
	exit;
}
$oprnd1 = trim($argv[1]);
$optor = trim($argv[2]);
$oprnd2 = trim($argv[3]);

if ($optor == "+")
	echo((string)((int)$oprnd1 + (int)$oprnd2)."\n");
else if ($optor == "-")
	echo((string)((int)$oprnd1 - (int)$oprnd2)."\n");
else if ($optor == "*")
	echo((string)((int)$oprnd1 * (int)$oprnd2)."\n");
else if ($optor == "/")
	echo((string)((int)$oprnd1 / (int)$oprnd2)."\n");
else if ($optor == "%")
	echo((string)((int)$oprnd1 % (int)$oprnd2)."\n");
?>
