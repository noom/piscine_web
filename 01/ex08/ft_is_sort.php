#!/usr/bin/php
<?php
function ft_is_sort($arr) {
	$greater = -1;
	foreach ($arr as $key => $val) {
		if ($key > 0 && $arr[$key - 1] < $arr[$key])
			$greater = 0;
		else if ($key > 0 && $arr[$key - 1] > $arr[$key])
			$greater = 1;
		if ($greater > -1)
			break;
	}
	if ($greater == -1)
		return true;
	foreach ($arr as $key => $val) {
		if ($greater == 1 && $key > 0 && $arr[$key - 1] < $arr[$key])
			return false;
		if ($greater == 0 && $key > 0 && $arr[$key - 1] > $arr[$key])
			return false;
	}
	return true;
}
?>
