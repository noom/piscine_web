#!/usr/bin/php
<?php
$stdin = fopen("php://stdin", "r");
if (!$stdin)
	exit();
$input = true;
while ($input != NULL)
{
	echo("Entrez un nombre: ");
	$input = fgets($stdin);
	if ($input == NULL)
		break;
	$input_trim = trim($input);
	if (is_numeric($input_trim))
	{
		if ($input_trim % 2 == 0)
			echo("Le chiffre $input_trim est Pair\n");
		else
			echo("Le chiffre $input_trim est Impair\n");
	}
	else
		echo("'$input_trim' n'est pas un chiffre\n");
}
fclose($stdin);
?>
