#!/usr/bin/php
<?php
function ft_split($tosplit) {
	$tosplit = trim(preg_replace("/[\\x00-\\x20]+/", " ", $tosplit));
	$exploded = explode(" ", $tosplit);
	sort($exploded);
	return $exploded;
}
?>
