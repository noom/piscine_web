#!/usr/bin/php
<?php
function ft_split($tosplit) {
	$tosplit = trim(preg_replace("/[\\x00-\\x20]+/", " ", $tosplit));
	$exploded = explode(":", $tosplit);
	return $exploded;
}

if ($argc < 3)
	exit;
$argv_reversed = array_reverse($argv);
$argv_len = count($argv_reversed);
foreach ($argv_reversed as $key => $val) {
	if ($key == $argv_len - 1 || $key == $argv_len - 2)
		break;
	$splited = ft_split($val);
	if (strcmp($argv_reversed[count($argv_reversed) - 2], $splited[0]) == 0
		&& count($splited) > 1) {
		echo("$splited[1]\n");
		break;
	}
}


