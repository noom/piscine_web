#!/usr/bin/php
<?php
if ($argc < 2) {
	echo("Incorrect Parameters\n");
	exit;
}
$input = trim(preg_replace("/[\\x00-\\x20]+/", "", $argv[1]));
list($oprnd1, $optor, $oprnd2) = sscanf($input, "%d%c%d");
$oprnd1 = (string)$oprnd1;
$oprnd2 = (string)$oprnd2;
if ($oprnd1 == "" || $optor == "" || $oprnd2 == "") {
	echo("Syntax Error\n");
	exit;
}
echo((string)eval("return $oprnd1 $optor $oprnd2;")."\n");
?>
