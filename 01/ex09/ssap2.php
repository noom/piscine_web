#!/usr/bin/php
<?php
function ft_split($tosplit) {
	$tosplit = trim(preg_replace("/[\\x00-\\x20]+/", " ", $tosplit));
	$exploded = explode(" ", $tosplit);
	return $exploded;
}

function get_char_type($c)
{
	if (ctype_alpha($c))
		return 1;
	if (ctype_digit($c))
		return 2;
	else
		return 3;
}

$mysort = function($s1, $s2) {
	$s1 = str_split($s1);
	$s2 = str_split($s2);
	$s1_len = count($s1);
	$s2_len = count($s2);
	$i = 0;
	while (($i < $s1_len) && ($i < $s2_len) && ( $s1[$i] == $s2[$i]))
		$i++;
	$c1_type = get_char_type($s1[$i]);
	$c2_type = get_char_type($s2[$i]);
	if ($c1_type != $c2_type)
		return $c1_type - $c2_type;
	else {
		$c1_lower = strtolower($s1[$i]);
		$c2_lower = strtolower($s2[$i]);
		return ord($c1_lower) - ord($c2_lower);
	}
};

$i = 1;
$final_array = array();
while ($i < $argc) {
	$to_add = ft_split($argv[$i]);
	foreach($to_add as $key => $val) {
		array_push($final_array, $val);
	}
	$i++;
}
usort($final_array, $mysort);
$len = count($final_array);
foreach ($final_array as $val) {
	echo("$val\n");
}
?>
