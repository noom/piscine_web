#!/usr/bin/php
<?php
function ft_split($tosplit) {
	$tosplit = trim(preg_replace("/[\\x00-\\x20]+/", " ", $tosplit));
	$exploded = explode(" ", $tosplit);
	return $exploded;
}

$i = 1;
$final_array = array();
while ($i < $argc) {
	$to_add = ft_split($argv[$i]);
	foreach($to_add as $key => $val) {
		array_push($final_array, $val);
	}
	$i++;
}
sort($final_array);
$len = count($final_array);
foreach ($final_array as $val) {
	echo("$val\n");
}
?>
