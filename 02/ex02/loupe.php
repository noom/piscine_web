#!/usr/bin/php
<?php

if ($argc < 2 || file_exists($argv[1]) == false || is_readable($argv[1]) == false)
	exit;
$file_content = file_get_contents($argv[1]);
$touppercase = function($match) {
	return strtoupper($match[0]);
};

$new_page = preg_replace_callback("/((?<=title=\")[^\"]*)/", $touppercase, $file_content);
$new_page = preg_replace_callback("/(<a\s*(.)*\\s*?>\\s*)(.)*?(?:\\s*<\\/a>)/", function($match) use($touppercase) {
	return preg_replace_callback("/>\s*(.)*?\s*</", $touppercase, $match[0]);
}, $new_page);
echo("$new_page\n");
?>
