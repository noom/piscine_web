#!/usr/bin/php
<?php
if ($argc < 2)
	exit;
$translate_arr = array("Lundi"=>"Monday", "Mardi"=>"Tuesday",
	"Mercredi"=>"Wednesday", "Jeudi"=>"Thursday", "Vendredi"=>"Friday",
	"Samedi"=>"Saturday", "Dimanche"=>"Sunday",
	"Janvier"=>"January", "Fevrier"=>"February", "Février"=>"February",
	"Mars"=>"March", "Avril"=>"April", "Mai"=>"May", "Juin"=>"June",
	"Juillet"=>"July", "Aout"=>"August", "Août"=>"August",
	"Septembre"=>"September", "Octobre"=>"October", "Novembre"=>"November",
	"Decembre"=>"December", "Décembre"=>"December");
$frenchified = strtr($argv[1], $translate_arr);
date_default_timezone_set("Europe/Paris");
if (($time = strtotime($frenchified)) == true)
	print("$time\n");
else
	print("Wrong Format\n");
?>
