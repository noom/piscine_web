<?php
abstract class Fighter {
	public $role;

	public function __construct($role) {
		$this->role = $role;
	}

	abstract public function fight($target);
}
?>
