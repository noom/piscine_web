<?php
class UnholyFactory {
	private $recruits = [];

	public function absorb($soldier) {
		if ($soldier instanceof Fighter) {
			foreach ($this->recruits as $recruit) {
				if ($recruit->role == $soldier->role) {
					echo("(Factory already absorbed a fighter of type foot soldier)\n");
					return ;
				}
			}
			echo("(Factory absorbed a fighter of type ". $soldier->role .")\n");
			$this->recruits[] = $soldier;
		}
		else
			echo("(Factory can't absorb this, it's not a fighter)\n");
	}

	public function fabricate($soldier_type) {
		$absorbed = false;
		foreach ($this->recruits as $recruit) {
			if ($recruit->role == $soldier_type)
				$absorbed = true;
		}
		if (!$absorbed)
			echo("(Factory hasn't absorbed any fighter of type ". $soldier_type .")\n");
		else
			echo("(Factory fabricates a fighter of type ". $soldier_type .")\n");
		foreach ($this->recruits as $recruit) {
			if ($recruit->role == $soldier_type)
				return clone $recruit;
		}
	}
}
?>
