<?php
class NightsWatch {
	private $recruits = array();

	public function recruit($who) {
		$this->recruits[] = $who;
	}

	public function fight() {
		foreach ($this->recruits as $recruit) {
			if ($recruit instanceof IFighter)
				$recruit->fight();
		}
	}
}
?>
