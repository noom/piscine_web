<?php
$file = explode("\n", file_get_contents("list.csv"));
$file_arr = Array();
foreach ($file as $line) {
	if (empty($line))
		continue;
	$line_exploded = explode(";", $line, 2);
	$file_arr[$line_exploded[0]] = $line_exploded[1];
}
echo(json_encode($file_arr));
?>
