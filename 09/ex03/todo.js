$(function() {
	$(document).ready(function() {
		$.get("/select.php", function(data, _) {
			var todos = JSON.parse(data);
			for (var i in todos) {
				var newtodo = $("<div>")
					.addClass("todos")
					.attr("id", i)
					.text(todos[i])
					.click(removeDiv);
				$('#ft_list').prepend(newtodo);
			}
		});
	});

	$('#new').click(function(ev) {
		var newtodo_text = prompt("Hey, type a new todo thing to add")
		if (newtodo_text.trim().length == 0)
			return;

		var newtodo = $("<div>")
			.addClass("todos")
			.text(newtodo_text)
			.click(removeDiv);
		$('#ft_list').prepend(newtodo);

		$.get("/insert.php?todo=" + newtodo_text, function(data, _) {
			newtodo.attr("id", data);
		});
	})

	function removeDiv(ev) {
		if (confirm("Do you really want to remove this item?")) {
			$(this).remove();

			$.get("/delete.php?id=" + ev.target.id);
		}
	}
})
