<?php
if (empty($_GET["id"])) {
	die();
}

$file = explode("\n", file_get_contents("list.csv"));
$file_arr = Array();
foreach ($file as $line) {
	if (empty($line))
		continue;
	$line_exploded = explode(";", $line, 2);
	if ($line_exploded[0] != $_GET["id"])
		$file_arr[$line_exploded[0]] = $line_exploded[0] . ";" . $line_exploded[1];
}
file_put_contents("list.csv", implode("\n", $file_arr) . "\n");
?>
