<?php
if (empty($_GET["todo"])) {
	die();
}

$file = explode("\n", file_get_contents("list.csv"));
$file_arr = Array();
$last_id = 0;
foreach ($file as $line) {
	if (empty($line))
		continue;
	$line_exploded = explode(";", $line, 2);
	$file_arr[$line_exploded[0]] = $line_exploded[1];
	$last_id = $line_exploded[0];
}

file_put_contents("list.csv", $last_id + 1 . ";" . $_GET["todo"] . "\n", FILE_APPEND);
echo($last_id + 1);
?>
