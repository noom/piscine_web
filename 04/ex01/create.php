<?php
session_start();

if ($_POST["submit"] != "OK" || $_POST["login"] == "" || $_POST["passwd"] == "") {
	echo("ERROR\n");
	exit;
}

if (file_exists("../private"))
	$passwd = unserialize(file_get_contents("../private/passwd"));
else
	$passwd = array();

$new_account = array(
	"login" => $_POST["login"],
	"passwd" => hash("whirlpool", $_POST["passwd"]));

foreach ($passwd as $key => $val) {
	if ($val["login"] == $new_account["login"]) {
		echo("ERROR\n");
		exit;
	}
}

$passwd[] = $new_account;

if (!file_exists("../private"))
	mkdir("../private");

$passwd = serialize($passwd);

file_put_contents("../private/passwd", $passwd);
echo("OK\n");
?>
