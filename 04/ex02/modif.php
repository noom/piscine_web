<?php
session_start();

if ($_POST["submit"] != "OK" || $_POST["login"] == "" || $_POST["oldpw"] == ""
	|| $_POST["newpw"] == "") {
	echo("ERROR\n");
	exit;
}

$passwd = unserialize(file_get_contents("../private/passwd"));

$pwok = false;
foreach ($passwd as $key => &$val) {
	if ($_POST["login"] == $val["login"] ) {
		if (hash("whirlpool", $_POST["oldpw"]) != $val["passwd"]) {
			echo("ERROR\n");
			exit;
		}
		$val["passwd"] = hash("whirlpool", $_POST["newpw"]);
		$pwok = true;
		break;
	}
}

if ($pwok == false) {
	echo("ERROR\n");
	exit;
}

$passwd = serialize($passwd);
file_put_contents("../private/passwd", $passwd);
echo("OK\n");
?>
